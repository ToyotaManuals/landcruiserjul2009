<?xml version="1.0"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<HEAD>
				<META http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<META http-equiv="Content-Style-Type" content="text/css" />
				<LINK REL="STYLESHEET" TYPE="text/css" HREF="menu.css"/>
				<script type="text/javascript">
					var bSelectedID = "";

					function selectItem(idSel)
					{
						nID = "id" + idSel;
						if( bSelectedID != "" ){	
							document.all(bSelectedID).className = "item";
						}
						document.all(nID).className = "selectedItem";
						bSelectedID = nID;
					}
				</script>
			</HEAD>
			<body>
				<xsl:apply-templates select="ConnList"/>
			</body>
		</html>

	</xsl:template>

	<xsl:template match="ConnList">
		<font size="-1"><b><xsl:value-of select="@sysname" /></b></font>
		<div class="itemList">
			<xsl:for-each select="item">
					<xsl:variable name="i" select="position()" />
				<xsl:if test="@type='conn'">
					<div class="item">
					<A class="item" target="PDF_Frame">
						<xsl:attribute name="id">id<xsl:value-of select="$i"/></xsl:attribute>
						<xsl:attribute name="onclick">selectItem(<xsl:value-of select="$i"/>)</xsl:attribute>
						<xsl:attribute name="href">../connector/<xsl:value-of select="partNo"/>.pdf</xsl:attribute>
						<xsl:value-of select="@code"/>
						<xsl:if test="note!=''">
							 &lt;<xsl:value-of select="note"/>&gt;
						</xsl:if>
					</A>
					</div>
				</xsl:if>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>
